package com.joasvpereira.codechallenge.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.objects.Languages;

/**
 * Created by Joás V. Pereira
 * on 22 Aug. 2018.
 */

public class JsonUtils {

  private static Gson gson;

  public static Gson getGsonUserResult() {
    return new GsonBuilder()
        .registerTypeAdapter(Languages.class, new LanguagesDeserializer())
        .create();
  }

  public static Gson getGson() {
    return gson = new GsonBuilder()
        .create();
  }

}
