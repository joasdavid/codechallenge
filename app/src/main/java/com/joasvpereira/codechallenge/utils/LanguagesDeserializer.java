package com.joasvpereira.codechallenge.utils;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.objects.Languages;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.objects.Languages.LanguageItem;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Joás V. Pereira
 * on 22 Aug. 2018.
 */

public class LanguagesDeserializer implements JsonDeserializer<Languages> {

  @Override
  public Languages deserialize(JsonElement element, Type type, JsonDeserializationContext context) throws JsonParseException {
    JsonObject jsonObject = element.getAsJsonObject();
    List<LanguageItem> languages = new ArrayList<LanguageItem>();
    for (Map.Entry<String, JsonElement> entry : jsonObject.entrySet()) {
      // For individual LanguageItem objects, we can use default deserialisation:
      LanguageItem languageItem = context.deserialize(entry.getValue(), LanguageItem.class);
      languageItem.setLanguageName(entry.getKey());
      languages.add(languageItem);
    }
    return new Languages(languages);
  }

}
