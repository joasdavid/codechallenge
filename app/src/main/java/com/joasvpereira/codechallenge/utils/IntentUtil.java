package com.joasvpereira.codechallenge.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/**
 * Created by Joás V. Pereira
 * on 22 Aug. 2018.
 */

public class IntentUtil {

  public static void openUrl(Context context, String url) {
    Uri link = Uri.parse(url);
    Intent myIntent = new Intent(Intent.ACTION_VIEW, link);
    myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    context.startActivity(myIntent);
  }

}
