package com.joasvpereira.codechallenge.di.modules;

import com.joasvpereira.codechallenge.logic.networking.models.get.results.UserResult;
import com.joasvpereira.codechallenge.logic.repository.challenges.authored.UseCaseAuthoredChallenge;
import com.joasvpereira.codechallenge.logic.repository.challenges.authored.UseCaseGetUserAuthoredChallengeInterface;
import com.joasvpereira.codechallenge.logic.repository.challenges.completed.UseCaseCompletedChallengeWithoutCache;
import com.joasvpereira.codechallenge.logic.repository.challenges.completed.UseCaseGetUserCompletedChallengeInterface;
import com.joasvpereira.codechallenge.logic.repository.details.UseCaseChallengeDetails;
import com.joasvpereira.codechallenge.logic.repository.details.UseCaseGetChallengeDetailsInterface;
import com.joasvpereira.codechallenge.logic.repository.users.UseCaseSearchUserByNameInterface;
import com.joasvpereira.codechallenge.logic.repository.users.UseCaseSearchUserByNameWithCache;
import com.joasvpereira.codechallenge.logic.repository.users.UseCaseSearchUserByNameWithoutCache;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Joás V. Pereira
 * on 19 Aug. 2018.
 */

@Module
public class RepositoryModule {

  @Provides
  public UseCaseSearchUserByNameInterface getUseCaseSearchUserByName(){
    //return new UseCaseSearchUserByNameWithoutCache();
    //switch the object to user one that caches the search users
    return new UseCaseSearchUserByNameWithCache();
  }


  @Provides
  public UseCaseGetUserCompletedChallengeInterface getUseCaseUserCompletedChallenge(){
    return new UseCaseCompletedChallengeWithoutCache();
  }

  @Provides
  public UseCaseGetUserAuthoredChallengeInterface getUseCaseUserAuthoredChallenge(){
    return new UseCaseAuthoredChallenge();
  }

  @Provides
  public UseCaseGetChallengeDetailsInterface getUseCaseChallengeDetails(){
    return new UseCaseChallengeDetails();
  }
}
