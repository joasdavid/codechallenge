package com.joasvpereira.codechallenge.di.componets;

import com.joasvpereira.codechallenge.di.modules.RepositoryModule;
import com.joasvpereira.codechallenge.logic.repository.CodeWarsRepository;
import com.joasvpereira.codechallenge.logic.repository.users.UseCaseSearchUserByNameInterface;
import dagger.Component;

/**
 * Created by Joás V. Pereira
 * on 19 Aug. 2018.
 */

@Component(modules = RepositoryModule.class)
public interface RepositoryComponent {
  void inject(CodeWarsRepository repository);
}
