package com.joasvpereira.codechallenge;

/**
 * Created by Joás V. Pereira
 * on 15 Aug. 2018.
 */

public class Configs {

  public static final String ENDPOINT = "http://www.codewars.com/api/v1/";
  public static final int NUMBER_OF_USER_TO_REMEMBER = 5;

}
