package com.joasvpereira.codechallenge;

import android.app.Application;
import android.arch.persistence.room.Room;
import com.joasvpereira.codechallenge.logic.database.AppDatabase;

/**
 * Created by Joás V. Pereira
 * on 20 Aug. 2018.
 */

public class MobileApplicationClass extends Application {

  private static AppDatabase db;
  private static MobileApplicationClass mobileApplicationClass;

  public static MobileApplicationClass getInstance() {
    return mobileApplicationClass;
  }

  public static AppDatabase getDataBase() {
    if (db == null) {
      db =
          Room
              .databaseBuilder(
                  mobileApplicationClass,
                  AppDatabase.class,
                  "db")
              .allowMainThreadQueries()
              .build();
    }
    return db;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    mobileApplicationClass = this;
  }
}
