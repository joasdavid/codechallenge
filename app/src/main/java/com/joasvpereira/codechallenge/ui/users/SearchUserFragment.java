package com.joasvpereira.codechallenge.ui.users;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.joasvpereira.codechallenge.R;
import com.joasvpereira.codechallenge.annotations.ScreenInfo;
import com.joasvpereira.codechallenge.constants.ShareDataConstants;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.UserResult;
import com.joasvpereira.codechallenge.ui.NavigationManagement;
import com.joasvpereira.codechallenge.ui.adapters.UsersAdapter;
import com.joasvpereira.codechallenge.ui.adapters.UsersAdapter.ItemSelectedInterface;
import com.joasvpereira.codechallenge.ui.bases.ScreenFragment;
import com.joasvpereira.codechallenge.ui.challenges.ChallengesFragment;
import java.util.List;

/**
 * Created by Joás V. Pereira
 * on 20 Aug. 2018.
 */

@ScreenInfo(
    title = R.string.fragment_title_search_user,
    layout = R.layout.fragment_search_user)
public class SearchUserFragment extends ScreenFragment {


  @BindView(R.id.searchEt)
  EditText searchEt;
  @BindView(R.id.userListRv)
  RecyclerView userListRv;
  @BindView(R.id.searchBt)
  Button searchBt;
  @BindView(R.id.loadingView)
  View loadingView;
  @BindView(R.id.searchByBt)
  Button searchByBt;
  Unbinder unbinder;

  private SearchUserViewModel viewModel;
  private boolean isByRank = false;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View rootView = super.onCreateView(inflater, container, savedInstanceState);
    assert rootView != null;
    unbinder = ButterKnife.bind(this, rootView);
    initUi();
    return rootView;
  }

  private void initUi() {
    viewModel = ViewModelProviders.of(this).get(SearchUserViewModel.class);

    viewModel.getIsLoading().observe(this, new Observer<Boolean>() {
      @Override
      public void onChanged(@Nullable Boolean aBoolean) {
        assert aBoolean != null;
        loadingView.setVisibility(
            aBoolean ? View.VISIBLE : View.GONE);
        ((ProgressBar) loadingView.findViewById(R.id.progressBar1)).setIndeterminate(aBoolean);
        searchBt.setEnabled(!aBoolean);
      }
    });

    viewModel.getMessages().observe(this, new Observer<String>() {
      @Override
      public void onChanged(@Nullable String s) {
        Toast.makeText(getContext(), s, Toast.LENGTH_SHORT).show();
      }
    });

    userListRv.setLayoutManager(new LinearLayoutManager(getContext()));
    //noinspection ConstantConditions
    userListRv.addItemDecoration(
        new DividerItemDecoration(getContext(), DividerItemDecoration.HORIZONTAL));

    viewModel.getUsersList(isByRank).observe(this, new Observer<List<UserResult>>() {
      @Override
      public void onChanged(@Nullable List<UserResult> userResults) {
        UsersAdapter adapter = new UsersAdapter(userResults, new ItemSelectedInterface() {
          @Override
          public void onItemClick(UserResult userResult) {
            getBaseActivity().getScreensShareData()
                .put(ShareDataConstants.USER, userResult.getUsername());
            NavigationManagement.addFragmentOnTop(
                getBaseActivity(),
                new ChallengesFragment());
          }
        });
        userListRv.setAdapter(adapter);
        searchByBt.setEnabled(((userResults != null ? userResults.size() : 0) >0));
      }
    });

  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    unbinder.unbind();
  }

  @OnClick({R.id.searchBt, R.id.searchByBt})
  public void onViewClicked(View view) {
    switch (view.getId()) {
      case R.id.searchBt:
        viewModel.getUser(searchEt.getText().toString());
        break;
      case R.id.searchByBt:
        isByRank = !isByRank;
        viewModel.getUsersList(isByRank);
        String text = (isByRank)
            ?getResources().getString(R.string.searched_by_rank)
            :getResources().getString(R.string.search_by_last_look_up);
        searchByBt.setText(text);
        break;
    }
  }
}
