package com.joasvpereira.codechallenge.ui.challenges.details;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.CodeChallengeResult;
import com.joasvpereira.codechallenge.logic.repository.CodeWarsRepository;
import com.joasvpereira.codechallenge.logic.repository.details.UseCaseGetChallengeDetailsInterface.UseCaseGetChallengeDetailsResultInterface;
import com.joasvpereira.codechallenge.ui.bases.ScreenBaseViewModel;
import java.util.List;

/**
 * Created by Joás V. Pereira
 * on 22 Aug. 2018.
 */

public class ChallengeDetailsViewModel extends ScreenBaseViewModel<CodeChallengeResult> implements
    UseCaseGetChallengeDetailsResultInterface{

  public ChallengeDetailsViewModel(
      @NonNull Application application) {
    super(application);
    repository = new CodeWarsRepository(this);
  }

  public LiveData<CodeChallengeResult> getDetails(String id){
    showLoading();
    repository.getChallenge(id);
    return getData();
  }

  @Override
  public void fetchChallengeDetailsSuccess(LiveData<CodeChallengeResult> challenge) {
    setData(challenge);
  }

  @Override
  public void fetchChallengeDetailsFailed(String errorMessage) {
    setError(errorMessage);
  }
}
