package com.joasvpereira.codechallenge.ui.challenges.details;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.joasvpereira.codechallenge.R;
import com.joasvpereira.codechallenge.annotations.ScreenInfo;
import com.joasvpereira.codechallenge.constants.ShareDataConstants;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.CodeChallengeResult;
import com.joasvpereira.codechallenge.ui.NavigationManagement;
import com.joasvpereira.codechallenge.ui.bases.ScreenFragment;
import com.joasvpereira.codechallenge.utils.IntentUtil;

/**
 * Created by Joás V. Pereira
 * on 22 Aug. 2018.
 */

@ScreenInfo(
    title = R.string.fragment_title_challenge,
    layout = R.layout.fragment_challenges_details)
public class ChallengeDetailsFragment extends ScreenFragment {

  @BindView(R.id.nameValueTv)
  TextView nameValueTv;
  @BindView(R.id.categoryValueTv)
  TextView categoryValueTv;
  @BindView(R.id.descriptionTv)
  TextView descriptionTv;
  @BindView(R.id.progressBar1)
  ProgressBar progressBar1;
  Unbinder unbinder;

  private ChallengeDetailsViewModel viewModel;
  private String link;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View rootView = super.onCreateView(inflater, container, savedInstanceState);
    unbinder = ButterKnife.bind(this, rootView);

    iniUI();

    return rootView;
  }

  private void iniUI() {
    viewModel = ViewModelProviders.of(this).get(ChallengeDetailsViewModel.class);

    viewModel.getIsLoading().observe(this, new Observer<Boolean>() {
      @Override
      public void onChanged(@Nullable Boolean aBoolean) {
        assert aBoolean != null;
        progressBar1.setVisibility(
            aBoolean ? View.VISIBLE : View.GONE);
        ((ProgressBar) progressBar1.findViewById(R.id.progressBar1)).setIndeterminate(aBoolean);

      }
    });

    viewModel.getMessages().observe(this, new Observer<String>() {
      @Override
      public void onChanged(@Nullable String s) {
        Toast.makeText(getContext(), s, Toast.LENGTH_SHORT).show();
      }
    });

    viewModel.getData().observe(this, new Observer<CodeChallengeResult>() {
      @Override
      public void onChanged(@Nullable CodeChallengeResult codeChallengeResult) {
        nameValueTv.setText(codeChallengeResult.getName());
        categoryValueTv.setText(codeChallengeResult.getCategory());
        descriptionTv.setText(codeChallengeResult.getDescription());
        link = codeChallengeResult.getUrl();
      }
    });

    if (getBaseActivity().getScreensShareData().containsKey(ShareDataConstants.CHALLENGE)) {
      viewModel.getDetails(
          (String) getBaseActivity().getScreensShareData().get(ShareDataConstants.CHALLENGE));
    } else {
      NavigationManagement.goBack(getBaseActivity());
    }
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    unbinder.unbind();
  }

  @OnClick(R.id.linkBt)
  public void onViewClicked() {
    IntentUtil.openUrl(getContext(), link);
  }
}
