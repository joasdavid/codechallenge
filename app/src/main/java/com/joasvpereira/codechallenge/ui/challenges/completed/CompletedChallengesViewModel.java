package com.joasvpereira.codechallenge.ui.challenges.completed;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import android.widget.Toast;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.CompletedChallengesResult;
import com.joasvpereira.codechallenge.logic.repository.CodeWarsRepository;
import com.joasvpereira.codechallenge.logic.repository.challenges.completed.UseCaseGetUserCompletedChallengeInterface.UseCaseGetUserCompletedChallengeResultInterface;
import com.joasvpereira.codechallenge.ui.bases.ScreenBaseViewModel;

/**
 * Created by Joás V. Pereira
 * on 21 Aug. 2018.
 */

public class CompletedChallengesViewModel extends
    ScreenBaseViewModel<CompletedChallengesResult> implements
    UseCaseGetUserCompletedChallengeResultInterface {

  public static final String ALL_PAGES_LOADED = "ALL_PAGES_LOADED";

  private String user;
  private int currentPage;
  private int totalPages;

  public CompletedChallengesViewModel(
      @NonNull Application application) {
    super(application);
    repository = new CodeWarsRepository(this);
    totalPages = 0;
  }

  public LiveData<CompletedChallengesResult> getCompletedChallenges(String user) {
    currentPage = 0;
    return getCompletedChallenges(user, currentPage);
  }

  private LiveData<CompletedChallengesResult> getCompletedChallenges(String user, int page) {
    this.user = user;
    showLoading();
    repository.getCompletedChallenge(user, page);
    return getData();
  }

  public void nextPage() {
    if (haveMorePages()) {
      repository.getCompletedChallenge(user, ++currentPage);
    }else {
      setMessages(ALL_PAGES_LOADED);
    }
  }

  private boolean haveMorePages() {
    return (currentPage < totalPages -1);
  }

  @Override
  public void fetchCompletedChallengeSuccess(
      LiveData<CompletedChallengesResult> completedChallenge) {
    if (completedChallenge.getValue() != null) {
      totalPages = completedChallenge.getValue().getTotalPages();
      setData(completedChallenge);
    }
  }

  @Override
  public void fetchCompletedChallengeFailed(String errorMessage) {
    setError(errorMessage);
  }
}
