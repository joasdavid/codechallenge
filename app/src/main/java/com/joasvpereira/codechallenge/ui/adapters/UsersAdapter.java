package com.joasvpereira.codechallenge.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.joasvpereira.codechallenge.R;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.UserResult;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.objects.Languages.LanguageItem;
import com.joasvpereira.codechallenge.ui.adapters.UsersAdapter.UserViewHolder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Joás V. Pereira
 * on 20 Aug. 2018.
 */


public class UsersAdapter extends RecyclerView.Adapter<UserViewHolder> {

  List<UserResult> userResultList;
  ItemSelectedInterface anInterface;


  @SuppressWarnings("unchecked")
  public UsersAdapter(List<UserResult> list, ItemSelectedInterface anInterface) {
    userResultList = list;
    this.anInterface = anInterface;
  }

  @NonNull
  @Override
  public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.adapter_item_user, parent, false);
    return new UserViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
    UserResult result = userResultList.get(position);

    holder.userNameValueTv.setText(result.getUsername());
    holder.rankValueTv.setText(String.valueOf(result.getRanks().getOverall().getName()));
    LanguageItem languageItem = getBestLanguage(result);
    String langName = languageItem.getLanguageName() + " : ";
    holder.languageTv.setText(langName);
    holder.languageValueTv.setText(String.valueOf(languageItem.getScore()));
  }

  private LanguageItem getBestLanguage(UserResult result ){
    List<LanguageItem> returnList = new ArrayList<>();
    returnList.addAll(result.getRanks().getLanguages().getLanguageItems());
    Collections.sort(returnList,
        new Comparator<LanguageItem>() {
          @Override
          public int compare(LanguageItem languageItem, LanguageItem t1) {
            return Integer.compare(
                t1.getScore(),
                languageItem.getScore()
            );
          }
        });
    return returnList.get(0);
  }


  @Override
  public int getItemCount() {
    return userResultList.size();
  }


  public class UserViewHolder extends ViewHolder {

    @BindView(R.id.userNameValueTv)
    TextView userNameValueTv;
    @BindView(R.id.rankValueTv)
    TextView rankValueTv;
    @BindView(R.id.scoreTV)
    TextView languageTv;
    @BindView(R.id.scoreValueTv)
    TextView languageValueTv;

    /**
     * ViewHolder.
     *
     * @param itemView View itemView
     */
    public UserViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
      itemView.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View view) {
          anInterface.onItemClick(userResultList.get(getAdapterPosition()));
        }
      });
    }

  }


  public interface ItemSelectedInterface{
    void onItemClick(UserResult userResult);
  }


}
