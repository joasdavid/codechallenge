package com.joasvpereira.codechallenge.ui.adapters.holders;

import android.view.View;

/**
 * Created by Joás V. Pereira
 * on 22 Aug. 2018.
 */

public interface AdapterItemInterface {

  void itemClicked(View view, int pos);

}
