package com.joasvpereira.codechallenge.ui.challenges.completed;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.joasvpereira.codechallenge.R;
import com.joasvpereira.codechallenge.annotations.ScreenInfo;
import com.joasvpereira.codechallenge.constants.ShareDataConstants;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.CompletedChallengesResult;
import com.joasvpereira.codechallenge.ui.NavigationManagement;
import com.joasvpereira.codechallenge.ui.adapters.CompletedChallengesAdapter;
import com.joasvpereira.codechallenge.ui.adapters.OnLoadMoreListener;
import com.joasvpereira.codechallenge.ui.adapters.holders.AdapterItemInterface;
import com.joasvpereira.codechallenge.ui.bases.ScreenFragment;
import com.joasvpereira.codechallenge.ui.challenges.details.ChallengeDetailsFragment;

/**
 * Created by Joás V. Pereira
 * on 21 Aug. 2018.
 */

@ScreenInfo(title = R.string.fragment_completed_challenges, layout = R.layout.fragment_challenges_completed)
public class CompletedChallengesFragment extends ScreenFragment {


  @BindView(R.id.completedChallengesRv)
  RecyclerView completedChallengesRv;
  @BindView(R.id.progressBar1)
  ProgressBar progressBar1;
  Unbinder unbinder;

  CompletedChallengesViewModel viewModel;
  CompletedChallengesAdapter adapter;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View rootView = super.onCreateView(inflater, container, savedInstanceState);
    assert rootView != null;
    unbinder = ButterKnife.bind(this, rootView);

    initUI();

    if (getBaseActivity().getScreensShareData().containsKey(ShareDataConstants.USER)) {
      viewModel.getCompletedChallenges(
          getBaseActivity().getScreensShareData().get(ShareDataConstants.USER).toString()
      );
    }

    return rootView;
  }

  private void initUI() {
    completedChallengesRv.setLayoutManager(new LinearLayoutManager(getContext()));
    //noinspection ConstantConditions
    completedChallengesRv.addItemDecoration(
        new DividerItemDecoration(getContext(), DividerItemDecoration.HORIZONTAL));

    adapter = new CompletedChallengesAdapter(completedChallengesRv, new AdapterItemInterface() {
      @Override
      public void itemClicked(View view, int pos) {
        String id =
            adapter.getCompletedChallengesList().get(pos).getId();
        getBaseActivity().getScreensShareData()
            .put(ShareDataConstants.CHALLENGE, id);
        NavigationManagement.addFragmentOnTop(getBaseActivity(), new ChallengeDetailsFragment());
      }
    });

    completedChallengesRv.setAdapter(adapter);

    adapter.setOnLoadMoreListener(new OnLoadMoreListener() {
      @Override
      public void onLoadMore() {
        viewModel.nextPage();
      }
    });

    viewModel = ViewModelProviders.of(this).get(CompletedChallengesViewModel.class);

    viewModel.getData().observe(this, new Observer<CompletedChallengesResult>() {
      @Override
      public void onChanged(@Nullable CompletedChallengesResult completedChallengesResult) {
        if (completedChallengesResult != null) {
          adapter.addCompletedChallenges(completedChallengesResult.getCompletedChallenge());
        }
      }
    });

    viewModel.getIsLoading().observe(this, new Observer<Boolean>() {
      @Override
      public void onChanged(@Nullable Boolean aBoolean) {
        assert aBoolean != null;
        progressBar1.setIndeterminate(aBoolean);
        progressBar1.setVisibility(aBoolean ? View.VISIBLE : View.GONE);
      }
    });

    viewModel.getMessages().observe(this, new Observer<String>() {
      @Override
      public void onChanged(@Nullable String s) {
        if (!TextUtils.isEmpty(s)) {
          if (s.equalsIgnoreCase(CompletedChallengesViewModel.ALL_PAGES_LOADED)) {
            adapter.notifyLastPageLoaded();
          } else {
            Toast.makeText(getContext(), s, Toast.LENGTH_LONG).show();
          }
        }
      }
    });

  }


  @Override
  public void onDestroyView() {
    super.onDestroyView();
    unbinder.unbind();
  }
}
