package com.joasvpereira.codechallenge.ui.bases;

/**
 * Created by Joás V. Pereira
 * on 20 Aug. 2018.
 */

public interface ScreenBackOnTopInterface {
    void backOnTop();
}
