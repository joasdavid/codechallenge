package com.joasvpereira.codechallenge.ui.users;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.UserResult;
import com.joasvpereira.codechallenge.logic.repository.CodeWarsRepository;
import com.joasvpereira.codechallenge.logic.repository.users.UseCaseSearchUserByNameInterface.UseCaseSearchUserByNameResultInterface;
import com.joasvpereira.codechallenge.ui.bases.ScreenBaseViewModel;
import java.util.List;

/**
 * Created by Joás V. Pereira
 * on 20 Aug. 2018.
 */

public class SearchUserViewModel extends ScreenBaseViewModel<List<UserResult>> implements
    UseCaseSearchUserByNameResultInterface {

  private static final int ORDER_BY_LAST_SEARCH = 0;
  private static final int ORDER_BY_RANK = 1;

  private MutableLiveData<List<UserResult>> fiveUsersList = new MutableLiveData<>();


  public SearchUserViewModel(@NonNull Application application) {
    super(application);
    repository = new CodeWarsRepository(((UseCaseSearchUserByNameResultInterface) this));
  }

  LiveData<List<UserResult>> getUser(String name) {
    showLoading();
    repository.getUser(name);
    return getData();
  }

  private void switchOrderBy(boolean isByRank) {
    showLoading();
    repository.getUserList(isByRank);
  }

  public LiveData<List<UserResult>> getUsersList(boolean isByRank) {
    switchOrderBy(isByRank);
    return getData();
  }

  @Override
  public void fetchUserByNameSuccess(LiveData<UserResult> userResult) {
    //Deprecated
  }


  @Override
  public void fetchFiveUsersSearched(LiveData<List<UserResult>> userResultList) {
    setData(userResultList);
  }

  @Override
  public void fetchUserByNameFailed(String errorMessage) {
    setError(errorMessage);
  }
}

