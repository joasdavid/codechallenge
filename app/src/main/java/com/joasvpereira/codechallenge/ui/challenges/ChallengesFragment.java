package com.joasvpereira.codechallenge.ui.challenges;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.BottomNavigationView.OnNavigationItemSelectedListener;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.joasvpereira.codechallenge.R;
import com.joasvpereira.codechallenge.annotations.ScreenInfo;
import com.joasvpereira.codechallenge.ui.NavigationManagement;
import com.joasvpereira.codechallenge.ui.bases.ScreenFragment;
import com.joasvpereira.codechallenge.ui.challenges.authored.AuthoredChallengesFragment;
import com.joasvpereira.codechallenge.ui.challenges.completed.CompletedChallengesFragment;

/**
 * Created by Joás V. Pereira
 * on 21 Aug. 2018.
 */
// TODO: 21/08/2018 add currect values
@ScreenInfo(title = R.string.app_name, layout = R.layout.challenges_container)
public class ChallengesFragment extends ScreenFragment {


  @BindView(R.id.bottom_navigation)
  BottomNavigationView bottomNavigation;
  @BindView(R.id.challenges_fragment_container)
  FrameLayout challengesFragmentContainer;
  Unbinder unbinder;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View rootView = super.onCreateView(inflater, container, savedInstanceState);
    unbinder = ButterKnife.bind(this, rootView);


    bottomNavigation.setOnNavigationItemSelectedListener(navigationOptions());
    bottomNavigation.setSelectedItemId(R.id.action_completed);
    return rootView;
  }

  private OnNavigationItemSelectedListener navigationOptions() {
    return new OnNavigationItemSelectedListener() {
      @Override
      public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
          case R.id.action_completed:
            NavigationManagement.replaceChildFragment(
                challengesFragmentContainer,
                new CompletedChallengesFragment(),
                getChildFragmentManager()
            );
            break;
          case R.id.action_authored:
            NavigationManagement.replaceChildFragment(
                challengesFragmentContainer,
                new AuthoredChallengesFragment(),
                getChildFragmentManager()
            );
            break;
        }

        return true;
      }
    };
  }



  @Override
  public void onDestroyView() {
    super.onDestroyView();
    unbinder.unbind();
  }
}
