package com.joasvpereira.codechallenge.ui.challenges.authored;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.objects.AuthoredChallenge;
import com.joasvpereira.codechallenge.logic.repository.CodeWarsRepository;
import com.joasvpereira.codechallenge.logic.repository.challenges.authored.UseCaseGetUserAuthoredChallengeInterface.UseCaseGetUserAuthoredChallengeResultInterface;
import com.joasvpereira.codechallenge.ui.bases.ScreenBaseViewModel;
import java.util.List;

/**
 * Created by Joás V. Pereira
 * on 22 Aug. 2018.
 */

public class AuthoredChallengesViewModel extends
    ScreenBaseViewModel<List<AuthoredChallenge>> implements
    UseCaseGetUserAuthoredChallengeResultInterface {

  public AuthoredChallengesViewModel(
      @NonNull Application application) {
    super(application);
    repository = new CodeWarsRepository(this);
  }

  public LiveData<List<AuthoredChallenge>> getAuthoredChallenges(String name) {
    showLoading();
    repository.getAuthoredChallenges(name);
    return getData();
  }

  @Override
  public void fetchAuthoredChallengeSuccess(LiveData<List<AuthoredChallenge>> authoredChallenge) {
    if (authoredChallenge.getValue() != null) {
      setData(authoredChallenge);
    }
  }

  @Override
  public void fetchAuthoredChallengeFailed(String errorMessage) {
    setError(errorMessage);
  }
}
