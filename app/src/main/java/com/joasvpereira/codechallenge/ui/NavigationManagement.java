package com.joasvpereira.codechallenge.ui;


import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import com.joasvpereira.codechallenge.R;
import com.joasvpereira.codechallenge.ui.bases.BaseActivity;
import com.joasvpereira.codechallenge.ui.bases.ScreenFragment;

/**
 * Created by Joás V. Pereira
 * on 17 Aug. 2018.
 */

@SuppressWarnings("ALL")
public class NavigationManagement {


  private static MutableLiveData<Boolean> isRootFragment = new MutableLiveData<>();

  @NonNull
  public static MutableLiveData<Boolean> getIsRootFragment() {
    return isRootFragment;
  }

  public static void addFragmentOnTop(@NonNull BaseActivity activity,
      @NonNull ScreenFragment nextFragment) {
    activity.hideKeyboard();

    Fragment currentFragment = activity.getSupportFragmentManager()
        .findFragmentById(R.id.base_fragment_container);

    String currentFragmentCanonicalName = null;
    if (currentFragment != null) {
      currentFragmentCanonicalName = currentFragment.getClass()
          .getCanonicalName();
    }
    String nextFragmentCanonicalName = nextFragment.getClass()
        .getCanonicalName();

    //to add a new fragment this have to be different or ate lest heve diferent arguments.
    if (currentFragment == null
        || !currentFragmentCanonicalName.equals(nextFragmentCanonicalName)
        || (currentFragmentCanonicalName.equals(nextFragmentCanonicalName)
        && nextFragment.getArguments() != null && currentFragment.getArguments() != null
        && !nextFragment.getArguments().equals(currentFragment.getArguments()))) {

      FragmentManager fm = activity.getSupportFragmentManager();

      isRootFragment.setValue(false);

      fm.beginTransaction()
          .add(R.id.base_fragment_container, nextFragment, nextFragmentCanonicalName)
          .addToBackStack(nextFragmentCanonicalName)
          .commit();

    }
  }

  private static void removeFragmentOnTop(BaseActivity activity, ScreenFragment removeFragment,
      FragmentManager fm) {

    fm.popBackStack(removeFragment.getClass().getCanonicalName(),
        FragmentManager.POP_BACK_STACK_INCLUSIVE);
    fm.beginTransaction().remove(removeFragment).commitNow();

    int size =
        fm.getFragments().size() - 1;
    Fragment fragment = fm.getFragments().get(size);

    if (fragment instanceof ScreenFragment) {
      activity.setTitle(
          ((ScreenFragment) fragment).getPageTitle()
      );
    }
    isRootFragment.setValue((size == 0));
  }

  public static void goBack(BaseActivity activity) {
    android.support.v4.app.FragmentManager fm = activity.getSupportFragmentManager();
    if (fm.getFragments().size() > 1) {
      removeFragmentOnTop(
          activity,
          (ScreenFragment) activity.getSupportFragmentManager()
              .findFragmentById(R.id.base_fragment_container),
          fm
      );
    }
  }


  public static void replaceChildFragment(int viewID, Fragment fragment,
      FragmentManager fragmentManager) {
    fragmentManager
        .beginTransaction()
        .replace(viewID, fragment)
        .commit();
  }

  public static void replaceChildFragment(View view, Fragment fragment,
      FragmentManager fragmentManager) {
    fragmentManager
        .beginTransaction()
        .replace(view.getId(), fragment)
        .commit();
  }
}
