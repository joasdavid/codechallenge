package com.joasvpereira.codechallenge.ui.adapters.holders;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.joasvpereira.codechallenge.R;
import com.joasvpereira.codechallenge.ui.NavigationManagement;

/**
 * Created by Joás V. Pereira
 * on 22 Aug. 2018.
 */

public class ChallengeViewHolder extends ViewHolder {

  @BindView(R.id.challengesNameTv)
  public TextView challengesNameTv;

  public ChallengeViewHolder(View view, @Nullable final AdapterItemInterface itemInterface) {
    super(view);
    ButterKnife.bind(this, view);
    view.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View view) {
        itemInterface.itemClicked(view,getAdapterPosition());
      }
    });
  }
}
