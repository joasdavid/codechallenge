package com.joasvpereira.codechallenge.ui.bases;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import com.joasvpereira.codechallenge.logic.repository.CodeWarsRepository;

/**
 * Created by Joás V. Pereira
 * on 20 Aug. 2018.
 */

public abstract class ScreenBaseViewModel<T extends Object> extends AndroidViewModel {

  private MutableLiveData<T> data = new MutableLiveData<>();
  protected CodeWarsRepository repository;
  private MutableLiveData<String> messages = new MutableLiveData<>();
  private MutableLiveData<Boolean> isLoading;

  public ScreenBaseViewModel(
      @NonNull Application application) {
    super(application);
  }

  public LiveData<T> getData() {
    return data;
  }

  public void setData(LiveData<T> data) {
    this.data.setValue(data.getValue());
    hideLoading();
  }

  protected void showLoading() {
    isLoading.setValue(true);
  }

  protected void hideLoading() {
    isLoading.setValue(false);
  }

  protected void setMessages(String errorMessage) {
    messages.setValue(errorMessage);
  }

  protected void setError(String errorMessage){
    setMessages(errorMessage);
    hideLoading();
  }

  public MutableLiveData<String> getMessages() {
    return messages;
  }

  public MutableLiveData<Boolean> getIsLoading() {
    if (isLoading == null) {
      isLoading = new MutableLiveData<>();
      isLoading.setValue(false);
    }
    return isLoading;
  }

  public void setIsLoading(MutableLiveData<Boolean> isLoading) {
    this.isLoading = isLoading;
  }
}
