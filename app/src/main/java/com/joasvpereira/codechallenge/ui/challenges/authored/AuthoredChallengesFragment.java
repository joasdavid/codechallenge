package com.joasvpereira.codechallenge.ui.challenges.authored;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.joasvpereira.codechallenge.R;
import com.joasvpereira.codechallenge.annotations.ScreenInfo;
import com.joasvpereira.codechallenge.constants.ShareDataConstants;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.objects.AuthoredChallenge;
import com.joasvpereira.codechallenge.ui.NavigationManagement;
import com.joasvpereira.codechallenge.ui.adapters.AuthoredChallengeAdapter;
import com.joasvpereira.codechallenge.ui.adapters.holders.AdapterItemInterface;
import com.joasvpereira.codechallenge.ui.bases.ScreenFragment;
import com.joasvpereira.codechallenge.ui.challenges.details.ChallengeDetailsFragment;
import java.util.List;

/**
 * Created by Joás V. Pereira
 * on 22 Aug. 2018.
 */

@ScreenInfo(title = R.string.fragment_authored_challenges, layout = R.layout.fragment_authored_challenges)
public class AuthoredChallengesFragment extends ScreenFragment {

  @BindView(R.id.authoredChallengesRv)
  RecyclerView authoredChallengesRv;
  @BindView(R.id.progressBar1)
  ProgressBar progressBar1;
  Unbinder unbinder;

  AuthoredChallengesViewModel viewModel;
  private AuthoredChallengeAdapter adapter;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View rootView = super.onCreateView(inflater, container, savedInstanceState);
    unbinder = ButterKnife.bind(this, rootView);

    initUI();

    return rootView;
  }

  private void initUI() {

    authoredChallengesRv.setLayoutManager(new LinearLayoutManager(getContext()));
    //noinspection ConstantConditions
    authoredChallengesRv.addItemDecoration(
        new DividerItemDecoration(getContext(), DividerItemDecoration.HORIZONTAL));

    viewModel = ViewModelProviders.of(this).get(AuthoredChallengesViewModel.class);

    viewModel.getIsLoading().observe(this, new Observer<Boolean>() {
      @Override
      public void onChanged(@Nullable Boolean aBoolean) {
        assert aBoolean != null;
        progressBar1.setIndeterminate(aBoolean);
        progressBar1.setVisibility(aBoolean ? View.VISIBLE : View.GONE);
      }
    });

    viewModel.getMessages().observe(this, new Observer<String>() {
      @Override
      public void onChanged(@Nullable String s) {
        Toast.makeText(getContext(), s, Toast.LENGTH_SHORT).show();
      }
    });

    if (getBaseActivity().getScreensShareData().containsKey(ShareDataConstants.USER)) {
      viewModel.getAuthoredChallenges(
          getBaseActivity().getScreensShareData().get(ShareDataConstants.USER).toString()
      ).observe(this, new Observer<List<AuthoredChallenge>>() {
        @Override
        public void onChanged(@Nullable List<AuthoredChallenge> authoredChallenges) {
          adapter = new AuthoredChallengeAdapter(authoredChallenges, new AdapterItemInterface() {
            @Override
            public void itemClicked(View view, int pos) {
              String id =
                  adapter.getList().get(pos).getId();
              getBaseActivity().getScreensShareData()
                  .put(ShareDataConstants.CHALLENGE, id);
              NavigationManagement.addFragmentOnTop(getBaseActivity(), new ChallengeDetailsFragment());
            }
          });
          authoredChallengesRv.setAdapter(adapter);
        }
      });
    }

  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    unbinder.unbind();
  }
}
