package com.joasvpereira.codechallenge.ui.bases;

import android.arch.lifecycle.Observer;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.joasvpereira.codechallenge.R;
import com.joasvpereira.codechallenge.ui.NavigationManagement;
import com.joasvpereira.codechallenge.ui.users.SearchUserFragment;
import java.util.HashMap;

/**
 * Created by Joás V. Pereira
 * on 16 Aug. 2018.
 *
 *
 * For this application i will implement a single entry point to the app, so Base activity will
 * work as an activity container for fragment that will represent the different functionalities
 * screens.
 */

public class BaseActivity extends AppCompatActivity {

  @BindView(R.id.base_fragment_container)
  FrameLayout baseFragmentContainer;

  HashMap<String, Object> screensShareData;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_base);
    ButterKnife.bind(this);

    NavigationManagement.getIsRootFragment().observe(this, new Observer<Boolean>() {
      @Override
      public void onChanged(@Nullable Boolean aBoolean) {
        if (aBoolean != null) {
          //noinspection ConstantConditions
          getSupportActionBar().setDisplayHomeAsUpEnabled(!aBoolean);
        }
      }
    });

    NavigationManagement.addFragmentOnTop(this, new SearchUserFragment());
  }

  @SuppressWarnings("ConstantConditions")
  @Override
  public void onBackPressed() {
    if (NavigationManagement.getIsRootFragment().getValue()) {
      finish();
    } else {
      NavigationManagement.goBack(this);
    }
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        onBackPressed();
        return false;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  /**
   * hideKeyboard.
   */
  public void hideKeyboard() {
    if (getCurrentFocus() != null) {
      InputMethodManager imm = (InputMethodManager) getSystemService(
          Context.INPUT_METHOD_SERVICE);
      if (imm != null) {
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
            InputMethodManager.HIDE_NOT_ALWAYS);
      }
    }
  }

  public HashMap<String, Object> getScreensShareData() {
    if (screensShareData == null) {
      screensShareData = new HashMap<>();
    }
    return screensShareData;
  }
}
