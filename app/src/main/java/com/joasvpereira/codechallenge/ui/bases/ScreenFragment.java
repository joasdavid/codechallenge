package com.joasvpereira.codechallenge.ui.bases;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.joasvpereira.codechallenge.annotations.ScreenInfo;

/**
 * Created by Joás V. Pereira
 * on 20 Aug. 2018.
 */

public abstract class ScreenFragment extends Fragment implements ScreenBackOnTopInterface{

  private View rootView;
  private int layout;
  private String pageTitle;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getClass().isAnnotationPresent(ScreenInfo.class)) {
      final ScreenInfo annotation = getClass().getAnnotation(ScreenInfo.class);
      setLayout(annotation.layout());
      setPageTitle(annotation.title());
    }

  }

  @Override
  public void onResume() {
    super.onResume();
    //noinspection ConstantConditions
    getActivity().setTitle(getPageTitle());
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    rootView = inflater.inflate(layout, container,
        false);
    return rootView;
  }

  @Override
  public void backOnTop() {
    //noinspection ConstantConditions
    getActivity().setTitle(getPageTitle());
  }

  public View getRootView() {
    return rootView;
  }

  public void setRootView(View rootView) {
    this.rootView = rootView;
  }

  public String getPageTitle() {
    return pageTitle;
  }

  public void setPageTitle(int pageTitleRes) {
    this.pageTitle = getResources().getString(pageTitleRes);
  }

  public int getLayout() {
    return layout;
  }

  public void setLayout(int layout) {
    this.layout = layout;
  }

  public BaseActivity getBaseActivity(){
    return (BaseActivity)getActivity();
  }
}
