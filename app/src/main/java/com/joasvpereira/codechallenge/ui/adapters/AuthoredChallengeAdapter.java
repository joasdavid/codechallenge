package com.joasvpereira.codechallenge.ui.adapters;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.joasvpereira.codechallenge.R;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.objects.AuthoredChallenge;
import com.joasvpereira.codechallenge.ui.adapters.holders.AdapterItemInterface;
import com.joasvpereira.codechallenge.ui.adapters.holders.ChallengeViewHolder;
import java.util.List;

/**
 * Created by Joás V. Pereira
 * on 22 Aug. 2018.
 */

public class AuthoredChallengeAdapter extends RecyclerView.Adapter<ChallengeViewHolder> {

  private List<AuthoredChallenge> list;
  private AdapterItemInterface adapterItemInterface;

  public AuthoredChallengeAdapter(
      List<AuthoredChallenge> list,
      @Nullable AdapterItemInterface adapterItemInterface) {
    this.list = list;
    this.adapterItemInterface = adapterItemInterface;
  }

  @NonNull
  @Override
  public ChallengeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.item_completed_challenges, parent, false);
    return new ChallengeViewHolder(view,adapterItemInterface);
  }

  @Override
  public void onBindViewHolder(@NonNull ChallengeViewHolder holder, int position) {
    AuthoredChallenge authoredChallenge = list.get(position);
    holder.challengesNameTv.setText(authoredChallenge.getName());
  }

  @Override
  public int getItemCount() {
    return list.size();
  }

  public List<AuthoredChallenge> getList() {
    return list;
  }
}
