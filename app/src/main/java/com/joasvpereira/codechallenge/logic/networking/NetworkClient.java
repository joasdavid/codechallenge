package com.joasvpereira.codechallenge.logic.networking;

import android.content.Context;
import android.net.NetworkInfo;
import android.util.Log;
import com.joasvpereira.codechallenge.Configs;
import com.joasvpereira.codechallenge.MobileApplicationClass;
import com.joasvpereira.codechallenge.logic.networking.api.RestApi;
import com.joasvpereira.codechallenge.utils.JsonUtils;
import java.io.File;
import java.io.IOException;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Joás V. Pereira
 * on 15 Aug. 2018.
 */

public class NetworkClient {

  private static Retrofit retrofit;

  private static boolean isConnected() {
    try {
      android.net.ConnectivityManager e = (android.net.ConnectivityManager) MobileApplicationClass
          .getInstance().getSystemService(
              Context.CONNECTIVITY_SERVICE);
      NetworkInfo activeNetwork = e.getActiveNetworkInfo();
      return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    } catch (Exception e) {
      Log.w("*****", e.toString());
    }

    return false;
  }

  /**
   * Init OkHttpClient and Retrofit.
   *
   * @return an instance of retrofit.
   */
  private static Retrofit getRetrofit() {

    if (retrofit == null) {

      File cacheFile = new File(
          MobileApplicationClass.getInstance().getCacheDir(),
          "okhttp_cache"
      );
      //cacheFile.mkdir();

      Cache cache = new Cache(cacheFile, 10 * 1000 * 1000);

      OkHttpClient.Builder builder = new OkHttpClient.Builder();
      builder.interceptors().add(
          new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
              Request originalRequest = chain.request();
              String cacheHeaderValue = isConnected()
                  ? "public, max-age=2419200"
                  : "public, only-if-cached, max-stale=2419200";
              Request request = originalRequest.newBuilder().build();
              Response response = chain.proceed(request);
              return response.newBuilder()
                  .removeHeader("Pragma")
                  .removeHeader("Cache-Control")
                  .header("Cache-Control", cacheHeaderValue)
                  .build();
            }
          }
      );
      builder.networkInterceptors().add(
              new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                  Request originalRequest = chain.request();
                  String cacheHeaderValue = isConnected()
                      ? "public, max-age=2419200"
                      : "public, only-if-cached, max-stale=2419200";
                  Request request = originalRequest.newBuilder().build();
                  Response response = chain.proceed(request);
                  return response.newBuilder()
                      .removeHeader("Pragma")
                      .removeHeader("Cache-Control")
                      .header("Cache-Control", cacheHeaderValue)
                      .build();
                }
              }
          );
      OkHttpClient okHttpClient = builder

          /*.addInterceptor(new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(@NonNull String message) {
              Log.d(NetworkClient.class.getName(), message);
            }
          }).setLevel(HttpLoggingInterceptor.Level.BODY))*/
          .cache(cache)
          .build();

      retrofit = new Retrofit.Builder()
          .baseUrl(Configs.ENDPOINT)
          .client(okHttpClient)
          .addConverterFactory(GsonConverterFactory.create(JsonUtils.getGsonUserResult()))
          .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
          .build();
    }

    return retrofit;
  }

  /**
   * Access point to the rest interface for calling services
   *
   * @return RestApi
   */
  public static RestApi getService() {
    return getRetrofit().create(RestApi.class);
  }



}
