package com.joasvpereira.codechallenge.logic.database.converters;

import android.arch.persistence.room.TypeConverter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.UserResult;
import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

/**
 * Created by Joás V. Pereira
 * on 22 Aug. 2018.
 */

public class UserConverter {
  static Gson gson = new Gson();

  @TypeConverter
  public static List<Object> stringToSomeObjectList(String data) {
    if (data == null) {
      return Collections.emptyList();
    }

    Type listType = new TypeToken<List<Object>>() {}.getType();

    return gson.fromJson(data, listType);
  }

  @TypeConverter
  public static String someObjectListToString(List<Object> someObjects) {
    return gson.toJson(someObjects);
  }
}
