package com.joasvpereira.codechallenge.logic.networking.models.get.results.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

public class CompletedChallenge {

  @Expose
  @SerializedName("completedLanguages")
  private List<String> completedLanguages;
  @Expose
  @SerializedName("completedAt")
  private String completedAt;
  @Expose
  @SerializedName("slug")
  private String slug;
  @Expose
  @SerializedName("name")
  private String name;
  @Expose
  @SerializedName("id")
  private String id;

  public List<String> getCompletedLanguages() {
    return completedLanguages;
  }

  public void setCompletedLanguages(List<String> completedLanguages) {
    this.completedLanguages = completedLanguages;
  }

  public String getCompletedAt() {
    return completedAt;
  }

  public void setCompletedAt(String completedAt) {
    this.completedAt = completedAt;
  }

  public String getSlug() {
    return slug;
  }

  public void setSlug(String slug) {
    this.slug = slug;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }
}
