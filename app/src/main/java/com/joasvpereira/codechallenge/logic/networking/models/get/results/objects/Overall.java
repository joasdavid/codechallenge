package com.joasvpereira.codechallenge.logic.networking.models.get.results.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Overall {

  @Expose
  @SerializedName("score")
  private int score;
  @Expose
  @SerializedName("color")
  private String color;
  @Expose
  @SerializedName("name")
  private String name;
  @Expose
  @SerializedName("rank")
  private int rank;

  public int getScore() {
    return score;
  }

  public void setScore(int score) {
    this.score = score;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getRank() {
    return rank;
  }

  public void setRank(int rank) {
    this.rank = rank;
  }
}
