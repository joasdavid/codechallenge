package com.joasvpereira.codechallenge.logic.repository.users;

import android.arch.lifecycle.MutableLiveData;
import com.joasvpereira.codechallenge.Configs;
import com.joasvpereira.codechallenge.MobileApplicationClass;
import com.joasvpereira.codechallenge.logic.database.entities.UserEntry;
import com.joasvpereira.codechallenge.logic.networking.NetworkClient;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.UserResult;
import com.joasvpereira.codechallenge.logic.repository.DataHandlerNotFoundException;
import com.joasvpereira.codechallenge.utils.JsonUtils;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Response;


/**
 * Created by Joás V. Pereira
 * on 22 Aug. 2018.
 */

public class UseCaseSearchUserByNameWithCache implements UseCaseSearchUserByNameInterface {

  private UseCaseSearchUserByNameResultInterface anInterface;
  private MutableLiveData<List<UserResult>> userResultListLiveData = new MutableLiveData<>();
  private List<UserResult> userResultList;
  private boolean isByRank;

  public UseCaseSearchUserByNameWithCache() {
    this(null);
  }

  public UseCaseSearchUserByNameWithCache(
      UseCaseSearchUserByNameResultInterface anInterface) {
    this.anInterface = anInterface;
    userResultList = new ArrayList<>();
  }


  @Override
  public void getUserByName(String name) throws DataHandlerNotFoundException {

    if (anInterface == null) {
      throw new DataHandlerNotFoundException();
    }

    Observable<Response<UserResult>> userResultObservable = NetworkClient
        .getService().getUserByName(name)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread());

    userResultObservable.subscribeWith(getObserver());
  }

  @Override
  public void getUserList(boolean isByRank) throws DataHandlerNotFoundException {
    if (anInterface == null) {
      throw new DataHandlerNotFoundException();
    }
    this.isByRank = isByRank;
    sendResultToView();

  }

  private void sendResultToView() {
    List<UserResult> sendList = new ArrayList<>();
    List<UserEntry> userEntries = (isByRank)
        ?MobileApplicationClass.getDataBase().userDao().getUsersRank()
        :MobileApplicationClass.getDataBase().userDao().getUsersById();
    for (UserEntry entry : userEntries) {
      sendList.add(entry.getUserResult());
    }

    userResultListLiveData.setValue(
        sendList
    );
    anInterface.fetchFiveUsersSearched(userResultListLiveData);

  }

  @Override
  public void setResultInterface(UseCaseSearchUserByNameResultInterface resultInterface) {
    anInterface = resultInterface;
  }


  private DisposableObserver<Response<UserResult>> getObserver() {
    return new DisposableObserver<Response<UserResult>>() {

      @Override
      public void onNext(@io.reactivex.annotations.NonNull Response<UserResult> userResponse) {
        if (userResponse.isSuccessful() && userResponse.code() == 200) {

          @SuppressWarnings("ConstantConditions")
          UserEntry userDb = new UserEntry(
              userResponse.body().getUsername(),
              JsonUtils.getGsonUserResult().toJson(userResponse.body()),
              userResponse.body().getLeaderboardPosition()
          );

          MobileApplicationClass.getDataBase().userDao()
              .insertUser(userDb);

          sendResultToView();
        } else {
          anInterface.fetchUserByNameFailed(userResponse.message());
        }
      }


      @Override
      public void onError(@io.reactivex.annotations.NonNull Throwable e) {
        anInterface.fetchUserByNameFailed(e.getMessage());
      }

      @Override
      public void onComplete() {

      }
    };
  }

}
