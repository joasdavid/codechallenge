package com.joasvpereira.codechallenge.logic.repository;

import com.joasvpereira.codechallenge.di.componets.DaggerRepositoryComponent;
import com.joasvpereira.codechallenge.di.componets.RepositoryComponent;
import com.joasvpereira.codechallenge.di.modules.RepositoryModule;
import com.joasvpereira.codechallenge.logic.repository.challenges.authored.UseCaseGetUserAuthoredChallengeInterface;
import com.joasvpereira.codechallenge.logic.repository.challenges.authored.UseCaseGetUserAuthoredChallengeInterface.UseCaseGetUserAuthoredChallengeResultInterface;
import com.joasvpereira.codechallenge.logic.repository.challenges.completed.UseCaseGetUserCompletedChallengeInterface;
import com.joasvpereira.codechallenge.logic.repository.challenges.completed.UseCaseGetUserCompletedChallengeInterface.UseCaseGetUserCompletedChallengeResultInterface;
import com.joasvpereira.codechallenge.logic.repository.details.UseCaseGetChallengeDetailsInterface;
import com.joasvpereira.codechallenge.logic.repository.details.UseCaseGetChallengeDetailsInterface.UseCaseGetChallengeDetailsResultInterface;
import com.joasvpereira.codechallenge.logic.repository.users.UseCaseSearchUserByNameInterface;
import com.joasvpereira.codechallenge.logic.repository.users.UseCaseSearchUserByNameInterface.UseCaseSearchUserByNameResultInterface;
import javax.inject.Inject;

/**
 * Created by Joás V. Pereira
 * on 18 Aug. 2018.
 *
 * This class is the data access to all CodeWars information allowing to fetch data as
 * users
 * user challenges
 * Completed
 *
 * - and more in the future -
 *
 * the main responsibility of this class is to relay communications with UI and the data models, as
 * so it dons't have any knowledge as the data is fetch living that responsibility to the use cases
 * loaded on it.
 */

public class CodeWarsRepository {

  @SuppressWarnings("WeakerAccess")
  @Inject
  public UseCaseGetUserCompletedChallengeInterface completedChallenges;
  private UseCaseGetUserCompletedChallengeResultInterface completedChallengesInterface;

  @SuppressWarnings("WeakerAccess")
  @Inject
  public UseCaseSearchUserByNameInterface searchUser;
  private UseCaseSearchUserByNameResultInterface searchUserInterface;

  @SuppressWarnings("WeakerAccess")
  @Inject
  public UseCaseGetUserAuthoredChallengeInterface authoredChallenge;
  private UseCaseGetUserAuthoredChallengeResultInterface authoredChallengeInterface;

  @SuppressWarnings("WeakerAccess")
  @Inject
  public UseCaseGetChallengeDetailsInterface challenge;
  private UseCaseGetChallengeDetailsResultInterface challengeInterface;

  public CodeWarsRepository(UseCaseResultInterface... interfaces) {
    RepositoryComponent component =
        DaggerRepositoryComponent.builder().repositoryModule(new RepositoryModule()).build();
    component.inject(this);

    for (UseCaseResultInterface useCase : interfaces) {
      if (useCase instanceof UseCaseSearchUserByNameResultInterface) {
        searchUserInterface = ((UseCaseSearchUserByNameResultInterface) useCase);
        searchUser.setResultInterface(
            searchUserInterface
        );
      }
      if (useCase instanceof UseCaseGetUserCompletedChallengeResultInterface) {
        completedChallengesInterface = ((UseCaseGetUserCompletedChallengeResultInterface) useCase);
        completedChallenges.setResultInterface(
            completedChallengesInterface
        );
      }
      if (useCase instanceof UseCaseGetUserAuthoredChallengeResultInterface) {
        authoredChallengeInterface = ((UseCaseGetUserAuthoredChallengeResultInterface) useCase);
        authoredChallenge.setResultInterface(
            authoredChallengeInterface
        );
      }
      if (useCase instanceof UseCaseGetUserAuthoredChallengeResultInterface) {
        authoredChallengeInterface = ((UseCaseGetUserAuthoredChallengeResultInterface) useCase);
        authoredChallenge.setResultInterface(
            authoredChallengeInterface
        );
      }
      if (useCase instanceof UseCaseGetChallengeDetailsResultInterface) {
        challengeInterface = ((UseCaseGetChallengeDetailsResultInterface) useCase);
        challenge.setResultInterface(
            challengeInterface
        );
      }
    }

  }


  public void getUserList(boolean isByRank) {
    try {
      searchUser.getUserList(isByRank);
    } catch (DataHandlerNotFoundException e) {
      searchUserInterface.fetchUserByNameFailed(e.getMessage());
    } catch (Exception ex) {
      searchUserInterface.fetchUserByNameFailed("");
    }
  }

  public void getUser(String name) {
    try {
      searchUser.getUserByName(name);
    } catch (DataHandlerNotFoundException e) {
      searchUserInterface.fetchUserByNameFailed(e.getMessage());
    } catch (Exception ex) {
      searchUserInterface.fetchUserByNameFailed("");
    }
  }

  public void getCompletedChallenge(String name, int page) {
    try {
      completedChallenges.getCompletedChallenge(name, page);
    } catch (DataHandlerNotFoundException e) {
      completedChallengesInterface.fetchCompletedChallengeFailed(e.getMessage());
    } catch (Exception ex) {
      completedChallengesInterface.fetchCompletedChallengeFailed("");
    }
  }

  public void getAuthoredChallenges(String name) {
    try {
      authoredChallenge.getAuthoredChallenge(name);
    } catch (DataHandlerNotFoundException e) {
      authoredChallengeInterface.fetchAuthoredChallengeFailed(e.getMessage());
    } catch (Exception ex) {
      authoredChallengeInterface.fetchAuthoredChallengeFailed("");
    }
  }

  public void getChallenge(String id) {
    try {
      challenge.getChallengeDetails(id);
    } catch (DataHandlerNotFoundException e) {
      challengeInterface.fetchChallengeDetailsFailed(e.getMessage());
    } catch (Exception ex) {
      challengeInterface.fetchChallengeDetailsFailed("");
    }
  }

}
