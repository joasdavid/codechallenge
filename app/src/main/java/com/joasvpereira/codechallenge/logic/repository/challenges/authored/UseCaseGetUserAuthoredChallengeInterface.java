package com.joasvpereira.codechallenge.logic.repository.challenges.authored;

import android.arch.lifecycle.LiveData;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.objects.AuthoredChallenge;
import com.joasvpereira.codechallenge.logic.repository.DataHandlerNotFoundException;
import com.joasvpereira.codechallenge.logic.repository.UseCaseInterface;
import com.joasvpereira.codechallenge.logic.repository.UseCaseResultInterface;
import com.joasvpereira.codechallenge.logic.repository.challenges.authored.UseCaseGetUserAuthoredChallengeInterface.UseCaseGetUserAuthoredChallengeResultInterface;
import java.util.List;

/**
 * Created by Joás V. Pereira
 * on 22 Aug. 2018.
 */

public interface UseCaseGetUserAuthoredChallengeInterface extends
    UseCaseInterface<UseCaseGetUserAuthoredChallengeResultInterface> {

  void getAuthoredChallenge(String name) throws DataHandlerNotFoundException;

  interface UseCaseGetUserAuthoredChallengeResultInterface extends UseCaseResultInterface {

    void fetchAuthoredChallengeSuccess(LiveData<List<AuthoredChallenge>> authoredChallenge);

    void fetchAuthoredChallengeFailed(String errorMessage);
  }

}
