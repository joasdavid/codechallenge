package com.joasvpereira.codechallenge.logic.repository.challenges.completed;

import android.arch.lifecycle.MutableLiveData;
import com.joasvpereira.codechallenge.logic.networking.NetworkClient;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.CompletedChallengesResult;
import com.joasvpereira.codechallenge.logic.repository.DataHandlerNotFoundException;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * Created by Joás V. Pereira
 * on 18 Aug. 2018.
 */

public class UseCaseCompletedChallengeWithoutCache implements
    UseCaseGetUserCompletedChallengeInterface {

  private UseCaseGetUserCompletedChallengeResultInterface anInterface;
  private MutableLiveData<CompletedChallengesResult> completedChallengesResultLiveData = new MutableLiveData<>();

  public UseCaseCompletedChallengeWithoutCache() {
  }

  public UseCaseCompletedChallengeWithoutCache(
      UseCaseGetUserCompletedChallengeResultInterface anInterface) {
    this.anInterface = anInterface;
  }

  @Override
  public void getCompletedChallenge(String name, int page) throws DataHandlerNotFoundException {

    if (anInterface == null) {
      throw new DataHandlerNotFoundException();
    }

    Observable<Response<CompletedChallengesResult>> CompletedChallengesResultObservable = NetworkClient
        .getService().getUserCompletedChallenges(name, page)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread());

    CompletedChallengesResultObservable.subscribeWith(getObserver());
  }

  @Override
  public void setResultInterface(UseCaseGetUserCompletedChallengeResultInterface resultInterface) {
    anInterface = resultInterface;
  }

  private DisposableObserver<Response<CompletedChallengesResult>> getObserver() {
    return new DisposableObserver<Response<CompletedChallengesResult>>() {

      @Override
      public void onNext(
          @io.reactivex.annotations.NonNull Response<CompletedChallengesResult> userResponse) {
        if (userResponse.isSuccessful() && userResponse.code() == 200) {
          completedChallengesResultLiveData.setValue(userResponse.body());
          anInterface.fetchCompletedChallengeSuccess(completedChallengesResultLiveData);
        } else {
          anInterface.fetchCompletedChallengeFailed(userResponse.message());
        }
      }

      @Override
      public void onError(@io.reactivex.annotations.NonNull Throwable e) {
        anInterface.fetchCompletedChallengeFailed(e.getMessage());
      }

      @Override
      public void onComplete() {

      }
    };
  }
}
