package com.joasvpereira.codechallenge.logic.database.entities;

import android.annotation.SuppressLint;
import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;
import com.joasvpereira.codechallenge.logic.database.converters.UserConverter;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.UserResult;
import com.joasvpereira.codechallenge.utils.JsonUtils;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Joás V. Pereira
 * on 22 Aug. 2018.
 */
@Entity(tableName = "users")
public class UserEntry {

  @PrimaryKey
  @NonNull
  @ColumnInfo(name = "entry_id")
  private String uid = "John Doe";

  @ColumnInfo(name = "json")
  @TypeConverters(UserConverter.class)
  private String userResultJson;

  @ColumnInfo(name = "time")
  private String time;

  @ColumnInfo(name = "pos")
  private int pos = Integer.MAX_VALUE;



  public String getUid() {
    return uid;
  }

  public void setUid(String uid) {
    this.uid = uid;
  }

  public UserEntry(@NonNull String uid, String userResultJson, int pos) {
    this.uid = uid;
    this.userResultJson = userResultJson;
    this.pos = pos;
    time = getTime();
  }

  public void setTime(String time) {
    this.time = time;
  }

  public int getPos() {
    return pos;
  }

  public void setPos(int pos) {
    this.pos = pos;
  }

  public String getUserResultJson() {
    return userResultJson;
  }

  public void setUserResultJson(String userResultJson) {
    this.userResultJson = userResultJson;
  }

  public UserResult getUserResult() {
    try {
      return JsonUtils.getGson().getAdapter(UserResult.class).fromJson(userResultJson);
    } catch (IOException e) {
      e.printStackTrace();
      return null;
    }
  }

  public String getTime() {
    @SuppressLint("SimpleDateFormat")
    SimpleDateFormat dt1 = new SimpleDateFormat("yyyyMMddhhmmss");
    return dt1.format(Calendar.getInstance().getTime());
  }
}
