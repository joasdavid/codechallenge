package com.joasvpereira.codechallenge.logic.repository;

/**
 * Created by Joás V. Pereira
 * on 19 Aug. 2018.
 */

public interface UseCaseInterface<T> {

  void setResultInterface(T resultInterface);

}
