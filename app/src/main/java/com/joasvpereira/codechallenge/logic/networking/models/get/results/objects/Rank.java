package com.joasvpereira.codechallenge.logic.networking.models.get.results.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Rank {

  @Expose
  @SerializedName("color")
  private String color;
  @Expose
  @SerializedName("name")
  private String name;
  @Expose
  @SerializedName("id")
  private int id;

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }
}
