package com.joasvpereira.codechallenge.logic.networking.api;

/**
 * Created by Joás V. Pereira
 * on 15 Aug. 2018.
 */


import com.joasvpereira.codechallenge.logic.networking.models.get.results.AuthoredChallengesResult;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.CodeChallengeResult;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.CompletedChallengesResult;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.UserResult;
import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Joás V. Pereira
 * on 15 Aug. 2018.
 *
 * Codewars API at https://dev.codewars.com/#api-reference
 */

public interface RestApi {

  /**
   * GET interface to search for a user by name, returning the information about the same.
   * @param name name of the user.
   * @return UserResult with all user information.
   */
  @GET("users/{userName}")
  Observable<Response<UserResult>> getUserByName(
      @Path("userName") String name
  );

  /**
   * GET interface to retrieve completed challenges of a specific user.
   * @param name - Name of the user with completed challenges.
   * @param pageNumber - Target page of completed challenges.
   * @return CompletedChallengesResult with a list of completed challenges and paging information.
   */
  @GET("users/{userName}/code-challenges/completed")
  Observable<Response<CompletedChallengesResult>> getUserCompletedChallenges(
      @Path("userName") String name,
      @Query("page") int pageNumber
  );

  /**
   * GET interface to retrieve completed authored of a specific user.
   * @param name - Name of the user with authored challenges.
   * @return CompletedChallengesResult with a list of authored challenges.
   */
  @GET("users/{userName}/code-challenges/authored")
  Observable<Response<AuthoredChallengesResult>> getUserAuthoredChallenges(
      @Path("userName") String name
  );

  /**
   * GET interface to provide a information about one challenge.
   * @param codeChallengeId - id of a challenge.
   * @return all challenge information.
   */
  @GET("code-challenges/{codeChallengeId}")
  Observable<Response<CodeChallengeResult>>getCodeChallenge(
      @Path("codeChallengeId") String codeChallengeId
  );

}
