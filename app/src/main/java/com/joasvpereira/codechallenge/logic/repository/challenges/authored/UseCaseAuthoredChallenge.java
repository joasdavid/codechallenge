package com.joasvpereira.codechallenge.logic.repository.challenges.authored;

import android.arch.lifecycle.MutableLiveData;
import com.joasvpereira.codechallenge.logic.networking.NetworkClient;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.AuthoredChallengesResult;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.objects.AuthoredChallenge;
import com.joasvpereira.codechallenge.logic.repository.DataHandlerNotFoundException;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import java.util.List;
import retrofit2.Response;

/**
 * Created by Joás V. Pereira
 * on 22 Aug. 2018.
 */

public class UseCaseAuthoredChallenge implements UseCaseGetUserAuthoredChallengeInterface {

  private UseCaseGetUserAuthoredChallengeResultInterface
      anInterface;
  private MutableLiveData<List<AuthoredChallenge>>
      authoredChallengesResultLiveData = new MutableLiveData<>();

  public UseCaseAuthoredChallenge() {}


  @Override
  public void getAuthoredChallenge(String name) throws DataHandlerNotFoundException {
    if (anInterface == null) {
      throw new DataHandlerNotFoundException();
    }

    Observable<Response<AuthoredChallengesResult>> AuthoredChallengesResultObservable = NetworkClient
        .getService().getUserAuthoredChallenges(name)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread());

    AuthoredChallengesResultObservable.subscribeWith(getObserver());
  }

  @Override
  public void setResultInterface(UseCaseGetUserAuthoredChallengeResultInterface resultInterface) {
    anInterface = resultInterface;
  }

  public Observer<Response<AuthoredChallengesResult>> getObserver() {
    return new DisposableObserver<Response<AuthoredChallengesResult>>() {

      @Override
      public void onNext(
          @io.reactivex.annotations.NonNull Response<AuthoredChallengesResult> response) {
        if (response.isSuccessful() && response.code() == 200 && response.body() != null
            && response.body().getAuthoredChallenges() != null) {
          authoredChallengesResultLiveData.setValue(response.body().getAuthoredChallenges());
          anInterface.fetchAuthoredChallengeSuccess(authoredChallengesResultLiveData);
        } else {
          anInterface.fetchAuthoredChallengeFailed(response.message());
        }
      }

      @Override
      public void onError(@io.reactivex.annotations.NonNull Throwable e) {
        anInterface.fetchAuthoredChallengeFailed(e.getMessage());
      }

      @Override
      public void onComplete() {

      }
    };
  }
}
