package com.joasvpereira.codechallenge.logic.networking.models.get.results.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CodeChallenges {

  @Expose
  @SerializedName("totalCompleted")
  private int totalCompleted;
  @Expose
  @SerializedName("totalAuthored")
  private int totalAuthored;

  public int getTotalCompleted() {
    return totalCompleted;
  }

  public void setTotalCompleted(int totalCompleted) {
    this.totalCompleted = totalCompleted;
  }

  public int getTotalAuthored() {
    return totalAuthored;
  }

  public void setTotalAuthored(int totalAuthored) {
    this.totalAuthored = totalAuthored;
  }
}
