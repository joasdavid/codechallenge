package com.joasvpereira.codechallenge.logic.repository.challenges.completed;

import android.arch.lifecycle.LiveData;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.CompletedChallengesResult;
import com.joasvpereira.codechallenge.logic.repository.DataHandlerNotFoundException;
import com.joasvpereira.codechallenge.logic.repository.UseCaseInterface;
import com.joasvpereira.codechallenge.logic.repository.UseCaseResultInterface;
import com.joasvpereira.codechallenge.logic.repository.challenges.completed.UseCaseGetUserCompletedChallengeInterface.UseCaseGetUserCompletedChallengeResultInterface;

/**
 * Created by Joás V. Pereira
 * on 18 Aug. 2018.
 */

public interface UseCaseGetUserCompletedChallengeInterface extends
    UseCaseInterface<UseCaseGetUserCompletedChallengeResultInterface> {

  void getCompletedChallenge(String name, int page) throws DataHandlerNotFoundException;

  interface UseCaseGetUserCompletedChallengeResultInterface extends UseCaseResultInterface {

    void fetchCompletedChallengeSuccess(LiveData<CompletedChallengesResult> completedChallenge);

    void fetchCompletedChallengeFailed(String errorMessage);
  }

}
