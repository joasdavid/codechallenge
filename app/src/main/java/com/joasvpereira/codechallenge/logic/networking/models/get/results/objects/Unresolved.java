package com.joasvpereira.codechallenge.logic.networking.models.get.results.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Unresolved {

  @Expose
  @SerializedName("suggestions")
  private int suggestions;
  @Expose
  @SerializedName("issues")
  private int issues;

  public int getSuggestions() {
    return suggestions;
  }

  public void setSuggestions(int suggestions) {
    this.suggestions = suggestions;
  }

  public int getIssues() {
    return issues;
  }

  public void setIssues(int issues) {
    this.issues = issues;
  }
}
