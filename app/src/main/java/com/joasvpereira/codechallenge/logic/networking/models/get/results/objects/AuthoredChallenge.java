package com.joasvpereira.codechallenge.logic.networking.models.get.results.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

public class AuthoredChallenge {

  @Expose
  @SerializedName("languages")
  private List<String> languages;
  @Expose
  @SerializedName("tags")
  private List<String> tags;
  @Expose
  @SerializedName("rankName")
  private String rankName;
  @Expose
  @SerializedName("rank")
  private int rank;
  @Expose
  @SerializedName("description")
  private String description;
  @Expose
  @SerializedName("name")
  private String name;
  @Expose
  @SerializedName("id")
  private String id;

  public List<String> getLanguages() {
    return languages;
  }

  public void setLanguages(List<String> languages) {
    this.languages = languages;
  }

  public List<String> getTags() {
    return tags;
  }

  public void setTags(List<String> tags) {
    this.tags = tags;
  }

  public String getRankName() {
    return rankName;
  }

  public void setRankName(String rankName) {
    this.rankName = rankName;
  }

  public int getRank() {
    return rank;
  }

  public void setRank(int rank) {
    this.rank = rank;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }
}
