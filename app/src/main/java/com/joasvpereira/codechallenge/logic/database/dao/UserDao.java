package com.joasvpereira.codechallenge.logic.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import com.joasvpereira.codechallenge.logic.database.entities.UserEntry;
import java.util.List;

/**
 * Created by Joás V. Pereira
 * on 22 Aug. 2018.
 */

@Dao
public interface UserDao {

  @Query("select * from users" +
      " ORDER BY time DESC")
  List<UserEntry> getUsersById();

  @Query("select * from users" +
      " ORDER BY pos ASC")
  List<UserEntry> getUsersRank();


  @Insert(onConflict = OnConflictStrategy.REPLACE)
  void insertUser(UserEntry userEntry);

}
