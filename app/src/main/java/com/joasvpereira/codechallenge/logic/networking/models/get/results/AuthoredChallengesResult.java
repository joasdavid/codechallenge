package com.joasvpereira.codechallenge.logic.networking.models.get.results;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.objects.AuthoredChallenge;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Joás V. Pereira
 * on 15 Aug. 2018.
 */

public class AuthoredChallengesResult implements Serializable {

  @Expose
  @SerializedName("data")
  private List<AuthoredChallenge> authoredChallenges;

  public List<AuthoredChallenge> getAuthoredChallenges() {
    return authoredChallenges;
  }

  public void setAuthoredChallenges(
      List<AuthoredChallenge> authoredChallenges) {
    this.authoredChallenges = authoredChallenges;
  }
}
