package com.joasvpereira.codechallenge.logic.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import com.joasvpereira.codechallenge.logic.database.dao.UserDao;
import com.joasvpereira.codechallenge.logic.database.entities.UserEntry;

/**
 * Created by Joás V. Pereira
 * on 22 Aug. 2018.
 */

@Database(entities = {UserEntry.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
  public abstract UserDao userDao();
}
