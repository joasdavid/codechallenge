package com.joasvpereira.codechallenge.logic.networking.models.get.results;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.objects.CodeChallenges;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.objects.Ranks;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Joás V. Pereira
 * on 15 Aug. 2018.
 */

public class UserResult implements Serializable {

  @Expose
  @SerializedName("codeChallenges")
  private CodeChallenges codeChallenges;
  @Expose
  @SerializedName("ranks")
  private Ranks ranks;
  @Expose
  @SerializedName("skills")
  private List<String> skills;
  @Expose
  @SerializedName("leaderboardPosition")
  private int leaderboardPosition;
  @Expose
  @SerializedName("clan")
  private String clan;
  @Expose
  @SerializedName("honor")
  private int honor;
  @Expose
  @SerializedName("name")
  private String name;
  @Expose
  @SerializedName("username")
  private String username;

  public UserResult(CodeChallenges codeChallenges, Ranks ranks,
      List<String> skills, int leaderboardPosition, String clan, int honor, String name,
      String username) {
    this.codeChallenges = codeChallenges;
    this.ranks = ranks;
    this.skills = skills;
    this.leaderboardPosition = leaderboardPosition;
    this.clan = clan;
    this.honor = honor;
    this.name = name;
    this.username = username;
  }

  public CodeChallenges getCodeChallenges() {
    return codeChallenges;
  }

  public void setCodeChallenges(CodeChallenges codeChallenges) {
    this.codeChallenges = codeChallenges;
  }

  public Ranks getRanks() {
    return ranks;
  }

  public void setRanks(Ranks ranks) {
    this.ranks = ranks;
  }

  public List<String> getSkills() {
    return skills;
  }

  public void setSkills(List<String> skills) {
    this.skills = skills;
  }

  public int getLeaderboardPosition() {
    return leaderboardPosition;
  }

  public void setLeaderboardPosition(int leaderboardPosition) {
    this.leaderboardPosition = leaderboardPosition;
  }

  public String getClan() {
    return clan;
  }

  public void setClan(String clan) {
    this.clan = clan;
  }

  public int getHonor() {
    return honor;
  }

  public void setHonor(int honor) {
    this.honor = honor;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  @Override
  public String toString() {
    return "UserResult{" +
        "skills=" + skills +
        ", leaderboardPosition=" + leaderboardPosition +
        ", clan='" + clan + '\'' +
        ", honor=" + honor +
        ", name='" + name + '\'' +
        ", username='" + username + '\'' +
        '}';
  }
}
