package com.joasvpereira.codechallenge.logic.repository.details;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import com.joasvpereira.codechallenge.logic.networking.NetworkClient;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.CodeChallengeResult;
import com.joasvpereira.codechallenge.logic.repository.DataHandlerNotFoundException;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * Created by Joás V. Pereira
 * on 22 Aug. 2018.
 */

public class UseCaseChallengeDetails implements UseCaseGetChallengeDetailsInterface {


  private UseCaseGetChallengeDetailsResultInterface anInterface;
  private MutableLiveData<CodeChallengeResult> challengeResultLiveData = new MutableLiveData<>();

  @Override
  public void setResultInterface(UseCaseGetChallengeDetailsResultInterface resultInterface) {
    anInterface = resultInterface;
  }

  @Override
  public void getChallengeDetails(String id) throws DataHandlerNotFoundException {
    if (anInterface == null) {
      throw new DataHandlerNotFoundException();
    }
    Observable<Response<CodeChallengeResult>> AuthoredChallengesResultObservable = NetworkClient
        .getService().getCodeChallenge(id)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread());

    AuthoredChallengesResultObservable.subscribeWith(getObserver());
  }

  public Observer<Response<CodeChallengeResult>> getObserver() {
    return new DisposableObserver<Response<CodeChallengeResult>>() {

      @Override
      public void onNext(
          @io.reactivex.annotations.NonNull Response<CodeChallengeResult> response) {
        if (response.isSuccessful() && response.code() == 200 && response.body() != null) {
          challengeResultLiveData.setValue(response.body());
          anInterface.fetchChallengeDetailsSuccess(challengeResultLiveData);
        } else {
          anInterface.fetchChallengeDetailsFailed(response.message());
        }
      }

      @Override
      public void onError(@io.reactivex.annotations.NonNull Throwable e) {
        anInterface.fetchChallengeDetailsFailed(e.getMessage());
      }

      @Override
      public void onComplete() {

      }
    };
  }
}
