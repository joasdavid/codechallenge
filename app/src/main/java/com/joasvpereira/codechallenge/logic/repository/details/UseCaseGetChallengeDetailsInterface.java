package com.joasvpereira.codechallenge.logic.repository.details;

import android.arch.lifecycle.LiveData;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.CodeChallengeResult;
import com.joasvpereira.codechallenge.logic.repository.DataHandlerNotFoundException;
import com.joasvpereira.codechallenge.logic.repository.UseCaseInterface;
import com.joasvpereira.codechallenge.logic.repository.UseCaseResultInterface;
import com.joasvpereira.codechallenge.logic.repository.details.UseCaseGetChallengeDetailsInterface.UseCaseGetChallengeDetailsResultInterface;
import java.util.List;

/**
 * Created by Joás V. Pereira
 * on 22 Aug. 2018.
 */

public interface UseCaseGetChallengeDetailsInterface extends
    UseCaseInterface<UseCaseGetChallengeDetailsResultInterface> {

  void getChallengeDetails(String id) throws DataHandlerNotFoundException;;

  interface UseCaseGetChallengeDetailsResultInterface extends UseCaseResultInterface{
    void fetchChallengeDetailsSuccess(LiveData<CodeChallengeResult> challenge);

    void fetchChallengeDetailsFailed(String errorMessage);
  }
}
