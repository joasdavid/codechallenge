package com.joasvpereira.codechallenge.logic.networking.models.get.results;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.objects.ApprovedBy;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.objects.CreatedBy;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.objects.Rank;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.objects.Unresolved;
import java.util.List;

/**
 * Created by Joás V. Pereira
 * on 15 Aug. 2018.
 */

public class CodeChallengeResult {

  @Expose
  @SerializedName("unresolved")
  private Unresolved unresolved;
  @Expose
  @SerializedName("contributorsWanted")
  private boolean contributorsWanted;
  @Expose
  @SerializedName("tags")
  private List<String> tags;
  @Expose
  @SerializedName("voteScore")
  private int voteScore;
  @Expose
  @SerializedName("totalStars")
  private int totalStars;
  @Expose
  @SerializedName("totalCompleted")
  private int totalCompleted;
  @Expose
  @SerializedName("totalAttempts")
  private int totalAttempts;
  @Expose
  @SerializedName("description")
  private String description;
  @Expose
  @SerializedName("approvedBy")
  private ApprovedBy approvedBy;
  @Expose
  @SerializedName("createdBy")
  private CreatedBy createdBy;
  @Expose
  @SerializedName("createdAt")
  private String createdAt;
  @Expose
  @SerializedName("rank")
  private Rank rank;
  @Expose
  @SerializedName("url")
  private String url;
  @Expose
  @SerializedName("languages")
  private List<String> languages;
  @Expose
  @SerializedName("approvedAt")
  private String approvedAt;
  @Expose
  @SerializedName("publishedAt")
  private String publishedAt;
  @Expose
  @SerializedName("category")
  private String category;
  @Expose
  @SerializedName("slug")
  private String slug;
  @Expose
  @SerializedName("name")
  private String name;
  @Expose
  @SerializedName("id")
  private String id;

  public Unresolved getUnresolved() {
    return unresolved;
  }

  public void setUnresolved(Unresolved unresolved) {
    this.unresolved = unresolved;
  }

  public boolean getContributorsWanted() {
    return contributorsWanted;
  }

  public void setContributorsWanted(boolean contributorsWanted) {
    this.contributorsWanted = contributorsWanted;
  }

  public List<String> getTags() {
    return tags;
  }

  public void setTags(List<String> tags) {
    this.tags = tags;
  }

  public int getVoteScore() {
    return voteScore;
  }

  public void setVoteScore(int voteScore) {
    this.voteScore = voteScore;
  }

  public int getTotalStars() {
    return totalStars;
  }

  public void setTotalStars(int totalStars) {
    this.totalStars = totalStars;
  }

  public int getTotalCompleted() {
    return totalCompleted;
  }

  public void setTotalCompleted(int totalCompleted) {
    this.totalCompleted = totalCompleted;
  }

  public int getTotalAttempts() {
    return totalAttempts;
  }

  public void setTotalAttempts(int totalAttempts) {
    this.totalAttempts = totalAttempts;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public ApprovedBy getApprovedBy() {
    return approvedBy;
  }

  public void setApprovedBy(ApprovedBy approvedBy) {
    this.approvedBy = approvedBy;
  }

  public CreatedBy getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(CreatedBy createdBy) {
    this.createdBy = createdBy;
  }

  public String getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(String createdAt) {
    this.createdAt = createdAt;
  }

  public Rank getRank() {
    return rank;
  }

  public void setRank(Rank rank) {
    this.rank = rank;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public List<String> getLanguages() {
    return languages;
  }

  public void setLanguages(List<String> languages) {
    this.languages = languages;
  }

  public String getApprovedAt() {
    return approvedAt;
  }

  public void setApprovedAt(String approvedAt) {
    this.approvedAt = approvedAt;
  }

  public String getPublishedAt() {
    return publishedAt;
  }

  public void setPublishedAt(String publishedAt) {
    this.publishedAt = publishedAt;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public String getSlug() {
    return slug;
  }

  public void setSlug(String slug) {
    this.slug = slug;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }
}
