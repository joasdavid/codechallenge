package com.joasvpereira.codechallenge.logic.repository.users;

import android.arch.lifecycle.LiveData;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.UserResult;
import com.joasvpereira.codechallenge.logic.repository.DataHandlerNotFoundException;
import com.joasvpereira.codechallenge.logic.repository.UseCaseInterface;
import com.joasvpereira.codechallenge.logic.repository.UseCaseResultInterface;
import com.joasvpereira.codechallenge.logic.repository.users.UseCaseSearchUserByNameInterface.UseCaseSearchUserByNameResultInterface;
import java.util.List;

/**
 * Created by Joás V. Pereira
 * on 18 Aug. 2018.
 *
 * This interface represent a contract to get an user by passing his name.
 */

public interface UseCaseSearchUserByNameInterface extends
    UseCaseInterface<UseCaseSearchUserByNameResultInterface> {

  void getUserByName(String name) throws DataHandlerNotFoundException;
  void getUserList(boolean isByRank) throws DataHandlerNotFoundException;

  interface UseCaseSearchUserByNameResultInterface extends UseCaseResultInterface {
    void fetchUserByNameSuccess(LiveData<UserResult> userResult);

    void fetchFiveUsersSearched(LiveData<List<UserResult>> userResultList);

    void fetchUserByNameFailed(String errorMessage);
  }

}
