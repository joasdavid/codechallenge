package com.joasvpereira.codechallenge.logic.networking.models.get.results.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Ranks {

  @Expose
  @SerializedName("languages")
  private Languages languages;
  @Expose
  @SerializedName("overall")
  private Overall overall;

  public Languages getLanguages() {
    return languages;
  }

  public void setLanguages(Languages languages) {
    this.languages = languages;
  }

  public Overall getOverall() {
    return overall;
  }

  public void setOverall(Overall overall) {
    this.overall = overall;
  }
}
