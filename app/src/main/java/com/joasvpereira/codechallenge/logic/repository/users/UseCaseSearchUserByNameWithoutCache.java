package com.joasvpereira.codechallenge.logic.repository.users;

import android.arch.lifecycle.MutableLiveData;
import com.joasvpereira.codechallenge.Configs;
import com.joasvpereira.codechallenge.logic.networking.NetworkClient;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.UserResult;
import com.joasvpereira.codechallenge.logic.repository.DataHandlerNotFoundException;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Response;

/**
 * Created by Joás V. Pereira on 18 Aug. 2018.
 *
 * Use case for fetch an user by a giving name.
 *
 * this class handles the fetching of user data throw a rest layer and interpreting the results to
 * be relaid back to the repository layer.
 *
 * this way all data fetching with success and response code = 200 are passed back throw a success
 * interface call with the body result, all other states are classified as an error and is passed
 * back an error message throw a fail interface call.
 */

public class UseCaseSearchUserByNameWithoutCache implements UseCaseSearchUserByNameInterface {

  private UseCaseSearchUserByNameResultInterface anInterface;
  private MutableLiveData<List<UserResult>> userResultListLiveData = new MutableLiveData<>();
  private List<UserResult> userResultList;

  public UseCaseSearchUserByNameWithoutCache() {
    this(null);
  }

  public UseCaseSearchUserByNameWithoutCache(
      UseCaseSearchUserByNameResultInterface anInterface) {
    this.anInterface = anInterface;
    userResultList = new ArrayList<>();
  }


  @Override
  public void getUserByName(String name) throws DataHandlerNotFoundException {

    if (anInterface == null) {
      throw new DataHandlerNotFoundException();
    }

    Observable<Response<UserResult>> userResultObservable = NetworkClient
        .getService().getUserByName(name)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread());

    userResultObservable.subscribeWith(getObserver());
  }

  @Override
  public void getUserList(boolean isByRank) throws DataHandlerNotFoundException {
    if (anInterface == null) {
      throw new DataHandlerNotFoundException();
    }

    returnFive();
  }

  @Override
  public void setResultInterface(UseCaseSearchUserByNameResultInterface resultInterface) {
    anInterface = resultInterface;
  }


  private DisposableObserver<Response<UserResult>> getObserver() {
    return new DisposableObserver<Response<UserResult>>() {

      @Override
      public void onNext(@io.reactivex.annotations.NonNull Response<UserResult> userResponse) {
        if (userResponse.isSuccessful() && userResponse.code() == 200) {
          //anInterface.fetchUserByNameSuccess(userResultListLiveData);
          userResultList.add(0, userResponse.body());
          returnFive();
        } else {
          anInterface.fetchUserByNameFailed(userResponse.message());
        }
      }

      @Override
      public void onError(@io.reactivex.annotations.NonNull Throwable e) {
        anInterface.fetchUserByNameFailed(e.getMessage());
      }

      @Override
      public void onComplete() {

      }
    };
  }

  private void returnFive() {
    List<UserResult> sendList = new ArrayList<>();
    int max = (userResultList.size() < Configs.NUMBER_OF_USER_TO_REMEMBER)
        ? userResultList.size()
        : Configs.NUMBER_OF_USER_TO_REMEMBER;

    for (int i = 0; i < max; i++) {
      if(!sendList.contains(userResultList.get(i)))
      sendList.add(userResultList.get(i));
    }

    userResultListLiveData.setValue(sendList);

    anInterface.fetchFiveUsersSearched(userResultListLiveData);
  }
}
