package com.joasvpereira.codechallenge.logic.repository;

/**
 * Created by Joás V. Pereira
 * on 18 Aug. 2018.
 */

public class DataHandlerNotFoundException extends Exception {

  public DataHandlerNotFoundException() {
    super("No data handler found for this use case!");
  }
}
