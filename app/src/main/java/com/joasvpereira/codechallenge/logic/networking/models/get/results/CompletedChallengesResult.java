package com.joasvpereira.codechallenge.logic.networking.models.get.results;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.objects.CompletedChallenge;
import java.io.Serializable;
import java.util.List;


/**
 * Created by Joás V. Pereira
 * on 15 Aug. 2018.
 */

public class CompletedChallengesResult implements Serializable {


  @Expose
  @SerializedName("data")
  private List<CompletedChallenge> CompletedChallenge;
  @Expose
  @SerializedName("totalItems")
  private int totalItems;
  @Expose
  @SerializedName("totalPages")
  private int totalPages;

  public List<CompletedChallenge> getCompletedChallenge() {
    return CompletedChallenge;
  }

  public void setCompletedChallenge(List<CompletedChallenge> CompletedChallenge) {
    this.CompletedChallenge = CompletedChallenge;
  }

  public int getTotalItems() {
    return totalItems;
  }

  public void setTotalItems(int totalItems) {
    this.totalItems = totalItems;
  }

  public int getTotalPages() {
    return totalPages;
  }

  public void setTotalPages(int totalPages) {
    this.totalPages = totalPages;
  }
}
