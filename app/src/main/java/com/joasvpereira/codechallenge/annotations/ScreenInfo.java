package com.joasvpereira.codechallenge.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Joás V. Pereira
 * on 20 Aug. 2018.
 *
 * This annotation will define the title to show when a specific fragment is on top and also the
 * layout to be used.
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ScreenInfo {

  int title() default -1;

  int layout() default -1;

}

