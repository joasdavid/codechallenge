package com.joasvpereira.codechallenge.constants;

/**
 * Created by Joás V. Pereira
 * on 22 Aug. 2018.
 */

public class ShareDataConstants {

  public static final String USER = "user";
  public static final String CHALLENGE = "challenge";

}
