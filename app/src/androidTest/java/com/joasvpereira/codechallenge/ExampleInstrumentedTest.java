package com.joasvpereira.codechallenge;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeThat;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;
import com.joasvpereira.codechallenge.logic.networking.NetworkClient;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.AuthoredChallengesResult;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.CodeChallengeResult;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.CompletedChallengesResult;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.UserResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import retrofit2.Response;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

  @Test
  public void useAppContext() throws Exception {
    // Context of the app under test.
    Context appContext = InstrumentationRegistry.getTargetContext();

    assertEquals("com.joasvpereira.codechallenge", appContext.getPackageName());
  }

  @Test
  public void getUserInformationTest() {
    Response<UserResult> response =
        NetworkClient.getService().getUserByName("cliffstamp").blockingFirst();
    assumeThat(response.isSuccessful(), is(true));
    assertEquals(response.body().getUsername(), "cliffstamp");
  }

  @Test
  public void getUserInformation404Test() {
    Response<UserResult> response =
        NetworkClient.getService().getUserByName("!#@£").blockingFirst();
    assertTrue(response != null);
  }

  @Test
  public void getUserCompletedChallengesTest() {
    Response<CompletedChallengesResult> response =
        NetworkClient.getService().getUserCompletedChallenges(
            "cliffstamp",
            0
        ).blockingFirst();
    assumeThat(response.isSuccessful(), is(true));
  }

  @Test
  public void getUserAuthoredChallengesTest() {
    Response<AuthoredChallengesResult> response =
        NetworkClient.getService().getUserAuthoredChallenges("cliffstamp").blockingFirst();
    assumeThat(response.isSuccessful(), is(true));
  }

  @Test
  public void getCodeChallengeTest() {
    Response<CodeChallengeResult> response =
        NetworkClient.getService().getCodeChallenge("573992c724fc289553000e95").blockingFirst();
    assertNotNull(response.body());
    //assumeThat(response.isSuccessful(), is(true));
    //assertEquals(response.body().getId(),"573992c724fc289553000e95");
  }
}
