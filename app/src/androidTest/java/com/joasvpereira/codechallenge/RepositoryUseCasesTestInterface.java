package com.joasvpereira.codechallenge;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.text.TextUtils;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.CompletedChallengesResult;
import com.joasvpereira.codechallenge.logic.networking.models.get.results.UserResult;
import com.joasvpereira.codechallenge.logic.repository.DataHandlerNotFoundException;
import com.joasvpereira.codechallenge.logic.repository.challenges.completed.UseCaseCompletedChallengeWithoutCache;
import com.joasvpereira.codechallenge.logic.repository.challenges.completed.UseCaseGetUserCompletedChallengeInterface.UseCaseGetUserCompletedChallengeResultInterface;
import com.joasvpereira.codechallenge.logic.repository.users.UseCaseSearchUserByNameInterface.UseCaseSearchUserByNameResultInterface;
import com.joasvpereira.codechallenge.logic.repository.users.UseCaseSearchUserByNameWithoutCache;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Created by Joás V. Pereira
 * on 18 Aug. 2018.
 */
@RunWith(AndroidJUnit4.class)
public class RepositoryUseCasesTestInterface {

  @Test
  public void useAppContext() throws Exception {
    // Context of the app under test.
    Context appContext = InstrumentationRegistry.getTargetContext();

    assertEquals("com.joasvpereira.codechallenge", appContext.getPackageName());
  }

  @Test
  public void UseCaseSearchUserByNameWithoutCacheSuccessTest() {
    UseCaseSearchUserByNameWithoutCache
        useCase = new UseCaseSearchUserByNameWithoutCache(
        new UseCaseSearchUserByNameResultInterface() {
          @Override
          public void fetchUserByNameSuccess(LiveData<UserResult> userResult) {

          }

          @Override
          public void fetchFiveUsersSearched(LiveData<List<UserResult>> userResultList) {
            assertEquals("cliffstamp", userResultList.getValue().get(0).getUsername());
          }

          @Override
          public void fetchUserByNameFailed(String errorMessage) {
            assertTrue(false);
          }
        });
    try {
      useCase.getUserByName("cliffstamp");
    } catch (DataHandlerNotFoundException e) {
      assertTrue(false);
    }
  }

  @Test
  public void UseCaseSearchUserByNameWithoutCacheFailTest() {
    UseCaseSearchUserByNameWithoutCache
        useCase = new UseCaseSearchUserByNameWithoutCache(
        new UseCaseSearchUserByNameResultInterface() {
          @Override
          public void fetchUserByNameSuccess(LiveData<UserResult> userResult) {
            assertTrue(false);
          }

          @Override
          public void fetchFiveUsersSearched(LiveData<List<UserResult>> userResultList) {
            assertTrue(false);
          }

          @Override
          public void fetchUserByNameFailed(String errorMessage) {
            assertTrue(!TextUtils.isEmpty(errorMessage));
          }
        });
    try {
      useCase.getUserByName("!#$@");
    } catch (DataHandlerNotFoundException e) {
      assertTrue(false);
    }
  }

  @Test
  public void UseCaseSearchUserByNameWithoutCacheExptionTest() {
    UseCaseSearchUserByNameWithoutCache
        useCase = new UseCaseSearchUserByNameWithoutCache();
    try {
      useCase.getUserByName("!#$@");
    } catch (DataHandlerNotFoundException e) {
      assertTrue(true);
    }
  }

  //---------------------------------------

  @Test
  public void UseCaseCompletedChallengeWithoutCacheSuccessTest() {
    UseCaseCompletedChallengeWithoutCache
        useCase = new UseCaseCompletedChallengeWithoutCache(
        new UseCaseGetUserCompletedChallengeResultInterface() {
          @Override
          public void fetchCompletedChallengeSuccess(
              LiveData<CompletedChallengesResult> completedChallenge) {
            assertNotNull(completedChallenge);
          }

          @Override
          public void fetchCompletedChallengeFailed(String errorMessage) {
            assertTrue(false);
          }
        });
    try {
      useCase.getCompletedChallenge("cliffstamp", 0);
    } catch (DataHandlerNotFoundException e) {
      assertTrue(false);
    }
  }

  @Test
  public void UseCasePage2CompletedChallengeWithoutCacheSuccessTest() {
    UseCaseCompletedChallengeWithoutCache
        useCase = new UseCaseCompletedChallengeWithoutCache(
        new UseCaseGetUserCompletedChallengeResultInterface() {
          @Override
          public void fetchCompletedChallengeSuccess(
              LiveData<CompletedChallengesResult> completedChallenge) {
            assertNotNull(completedChallenge);
          }

          @Override
          public void fetchCompletedChallengeFailed(String errorMessage) {
            assertTrue(false);
          }
        });
    try {
      useCase.getCompletedChallenge("cliffstamp", 1);
    } catch (DataHandlerNotFoundException e) {
      assertTrue(false);
    }
  }

  @Test
  public void UseCaseCompletedChallengeWithoutCacheFailTest() {
    UseCaseCompletedChallengeWithoutCache
        useCase = new UseCaseCompletedChallengeWithoutCache(
        new UseCaseGetUserCompletedChallengeResultInterface() {
          @Override
          public void fetchCompletedChallengeSuccess(
              LiveData<CompletedChallengesResult> completedChallenge) {
            assertTrue(false);
          }

          @Override
          public void fetchCompletedChallengeFailed(String errorMessage) {
            assertTrue(!TextUtils.isEmpty(errorMessage));
          }
        });
    try {
      useCase.getCompletedChallenge("!#$@", 0);
    } catch (DataHandlerNotFoundException e) {
      assertTrue(false);
    }
  }

  @Test
  public void UseCaseCompletedChallengeWithoutCacheExptionTest() {
    UseCaseCompletedChallengeWithoutCache
        useCase = new UseCaseCompletedChallengeWithoutCache();
    try {
      useCase.getCompletedChallenge("!#$@", 0);
    } catch (DataHandlerNotFoundException e) {
      assertTrue(true);
    }
  }

}
